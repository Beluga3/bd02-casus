import csv
import cv2
import itertools
import numpy as np
import pandas as pd             #<  pip install
import os
import sys
import tempfile
import tqdm                     #<  pip install

from matplotlib import pyplot as plt
from matplotlib.collections import LineCollection

import tensorflow as tf
import tensorflow_hub as hub    #<  pip install
from tensorflow import keras

from sklearn.model_selection import train_test_split    #<  pip install
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix

#===============Visualize pose estimation results=============================
def draw_prediction_on_image(
    image, person, crop_region=None, close_figure=True,
    keep_input_size=False):
    """
    Args:
        image: Numpy array with shape [height, width, channel] (= pixel values of input image)
        person: A person entity returned from the MoveNet.SinglePose model
        close_figure: Close the plt figure after function return or not
        keep_input_size: keep the size of the input image
    Returns:
        Numpy array with shape [out_height, out_width, channel] (= image with overlaid keypoints)
    """
    #draw detection result on image
    image_np = utils.visualize(image, [person])

    #plot image with results
    height, width, channel = image.shape
    aspect_ratio = float(width) / height
    fig, ax = plt.subplots(figsize=(12 * aspect_ratio, 12))
    im = ax.imshow(image_np)
    if close_figure:
        plt.close(fig)
    if not keep_input_size:
        image_np = utils.keep_aspect_ratio_resizer(image_np, (512, 512))

    return image_np

class MoveNetPreProcessor(object):
    def __init__(self,
                 images_in_folder,
                 images_out_folder,
                 cvs_out_path):
        """
        Args:
            images_in_folder: Path to folder with input images
            IMAGEFOLDER
            |__posename1
                |_________000001.jpg
                |_________000002.jpg
            |__posename2
                |_________000003.jpg
                |_________000004.jpg
            images_out_folder:  Path to write overlaid images to
            csvs_out_path: Path to write CSV with detected landmarks and labels to
        """
        self._images_in_folder = images_in_folder
        self._images_out_folder = images_out_folder
        self._csvs_out_path = csvs_out_path
        self._messages = []

        #temporary directory to store pose csvs per class
        self.csvs_out_folder_per_class = tempfile.mkdtemp()

        #get list of pose classes and print image statistics
        self._pose_class_names = sorted(
            [n for n in os.listdir(self._images_in_folder) if not n.startswith('.')]
            )

        def process(self, per_pose_class_limit=None, detection_threshold=0.1):
            """
            Preprocesses images in given folder
            Args:
                per_pose_class_limit: Number of images to load  (could be limited to reduce training time)
                detection_threshold: Only keep images with all landmark confidence score above this threshold.
            """
            for pose_class_name in self._pose_class_names:
                print('Preprocessing', pose_class_name, file=sys.stderr)

                #paths for pose class
                images_in_folder = os.path.join(self._images_in_folder, pose_class_name)
                images_out_folder = os.path.join(self._images_out_folder, pose_class_name)
                csv_out_path = os.path.join(self._csvs_out_folder_per_class, pose_class_name + '.csv')
                if not os.path.exists(images_out_folder):
                    os.makedirs(images_out_folder)

                #detect landmarks in each image and write it to a csv file
                with open(csv_out_path, 'w') as csv_out_file:
                    csv_out_writer = csv.writer(csv_out_file,
                                                delimiter=',',
                                                quoting=csv.QUOTE_MINIMAL)
                    #get list of images
                    image_names = sorted(
                        [n for n in os.listdir(images_in_folder) if not n.startswith('.')])
                    if per_pose_class_limit is not None:
                        image_names = image_names[:per_pose_class_limit]

                    valid_image_count = 0

                    #detect pose landmarks from each image
                    for image_name in tqdm.tqdm(image_names):
                        image_path = os.path.join(images_in_folder, image_name)

                        try:
                            image = tf.io.read_file(image_path)
                            image = tf.io.decode_jpeg(image)
                        except:
                            self._messages.append('Skipped ' + image_path + '. Invalid image.')
                            continue
                        else:
                            image = tf.io.read_file(image_path)
                            image = tf.io.decode_jpeg(image)
                            image_height, image_width, channel = image.shape

                        #skip non-rgb images because Movenet requires RGB images
                        if channel != 3:
                            self._messages.append('Skipped ' + image_path + '. Image isn\'t in RGB format. ')
                            continue
                        person = detect(image)

                        #save landmarks if all were detected
                        min_landmark_score = min(
                            [keypoint.score for keypoint in person.keypoints])
                        should_keep_image = min_landmark_score >= detection_threshold
                        if not should_keep_image:
                            self._messages.append('Skipped ' + image_path + '. No pose was confidently detected.')
                            continue
                        valid_image_count += 1

                        #draw prediction result on top of the image
                        output_overlay = draw_prediction_on_image(
                            image.numpy().astype(np.uint8), person, close_figure=True, keep_input_size=True)

                        #write detection result into image file
                        output_frame = cv2.cvtColor(output_overlay, cv2.COLOR_RGB2BGR)
                        cv2.imwrite(os.path.join(images_out_folder, image_name), output_frame)

                        #get landmarks and scale it to same size as input image
                        pose_landmarks = np.array(
                            [[keypoint.coordinate.x, keypoint.coordinate.y, keypoint.score]
                             for keypoint in person.keypoints],
                            dtype=np.float32)

                        #write landmark coordinates to its per-class CSV file
                        coordinates = pose_landmarks.flatten().astype(np.str).tolist()
                        csv_out_writer.writerow([image_name] + coordinates)

                    if not valid_image_count:
                        raise RuntimeError(
                            'No valid images found for the "{}" class.'.format(pose_class_name))

                #print the error messages collected during preprocessing
                print('\n'.join(self._messages))

                #combine all per-class CSVs into single output
                all_landmarks_df = self._all_landmarks_as_dataframe()
                all_landmarks_df.to_csv(self._csvs_out_path, index=False)

            def class_names(self):
                """List of classes found in training set"""
                return self._pose_class_names

            def _all_landmarks_as_dataframe(self):
                """Merge all per-class CSVs into single dataframe"""
                total_df = None
                for class_index, class_name in enumerate(self._pose_class_names):
                    csv_out_path = os.path.join(self._csvs_out_folder_per_class,
                                                class_name + '.csv')
                    per_class_df = pd.read_csv(csv_out_path, header=None)

                    #add labels
                    per_class_df['class_no'] = [class_index]*len(per_class_df)
                    per_class_df['class_name'] = [class_name]*len(per_class_df)

                    #append folder name to the filename column (first column)
                    per_class_df[per_class_df.columns[0]] = (os.path.join(class_name, '')
                                                             + per_class_df[per_class_df.columns[0]].astype(str))

                    if total_df is None:
                        #for first class, assign data to the total dataframe
                        total_df = per_class_df
                    else:
                        #concatenate each class's data into total dataframe
                        total_df = pd.concat([total_df, per_class_df], axis=0)

                list_name = [[bodypart.name + '_x', bodypart.name + '_y',
                              bodypart.name + '_score'] for bodypart in BodyPart]
                header_name = []
                for columns_name in list_name:
                    header_name += columns_name
                header_name = ['file_name'] + header_name
                header_map = {total_df.columns[i]: header_name[i]
                              for i in range(len(header_name))}
                total_df.rename(header_map, axis=1, inplace=True)

                return total_df

#Upload dataset
"""
images need to be split into train and test data
            IMAGEFOLDER
            |__posename1
                |_________000001.jpg
                |_________000002.jpg
            |__posename2
                |_________000003.jpg
                |_________000004.jpg
        use_custom_dataset = True for using own labeled poses
        dataset_is_split = False for using images and labels without manual split
"""
is_skip_step_1 = False
use_custom_dataset = True
dataset_is_split = False

if use_custom_dataset:
    print("""\
        FOLDER
        |__posename1/
            |_________00001.jpg
            |_________00002.jpg
        |__posename2/
            |_________00003.jpg
            |_________00004.jpg
          """)
    print("INFO: All images need to be of either PNG, JPEG or BMP file type. Other file types are ignored.")
    input("Prepare dataset folders in this directory as shown above with all images in this structure. \n afterwards, press ENTER.")
    f = input("Type the name of the FOLDER that contains all classes: ")
    dataset_in = f
    if not os.path.isdir(dataset_in):
        raise Exception("dataset_in is not a valid directory")
    if dataset_is_split:
        IMAGES_ROOT = dataset_in
    else:
        dataset_out = 'split_' + dataset_in
        split_into_train_test(dataset_in, dataset_out, test_split=0.2)
        IMAGES_ROOT = dataset_out

#Preprocess TRAIN dataset
if not is_skip_step_1:
    images_in_train_folder = os.path.join(IMAGES_ROOT, 'train')
    images_out_train_folder = 'poses_images_out_train'
    csvs_out_train_path = 'train_data.csv'

    preprocessor = MoveNetPreprocessor(
        images_in_folder=images_in_train_folder,
        images_out_folder=images_out_train_folder,
        csvs_out_path=csvs_out_train_path,
    )
    preprocessor.process(per_pose_class_limit=None)

#Preprocess TEST dataset
if not is_skip_step_1:
    images_in_test_folder = os.path.join(IMAGES_ROOT, 'test')
    images_out_test_folder = 'poses_images_out_test'
    csvs_out_test_path = 'test_data.csv'

    preprocessor = MoveNetPreprocessor(
        images_in_folder=images_in_test_folder,
        images_out_folder=images_out_test_folder,
        csvs_out_path=csvs_out_test_path,
    )
    preprocessor.process(per_pose_class_limit=None)

def load_pose_landmarks(csv_path):
    """Loads a CSV created by MoveNetPreprocessor.

    Returns:
        X: Detected landmark coordinates and scores of shape (N, 17 * 3)
        Y: Ground truth labels of shape (N, label_count)
        classes: The list of all class names found in the dataset
        dataframe: the CSV loaded as a Pandas dataframe features (X) and ground truth labels (y) to use later to train a pose classification model
    """
    #load CSV
    dataframe = pd.read_csv(csv_path)
    df_to_process = dataframe.copy()

    #drop file_name columns
    df_to_process.drop(columns=['file_name'], inplace=True)

    #extract list of class names
    classes = df_to_process.pop('class_name').unique()

    #extract labels
    y = df_to_process.pop('class_no')

    #convert input features and labels into correct format for training
    X = df_to_process.astype('float64')
    y = keras.utils.to_categorical(y)

    return X, y, classes, dataframe

#load train data
X, y, class_names, _ = load_pose_landmarks(csvs_out_train_path)
#split training data (X, y) into (X_train, y_train) and (X_val, y_val)
X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.15)
#load the test data
X_test, y_test, _, df_test = load_pose_landmarks(csvs_out_test_path)

"""Define functions to convert the pose landmarks to a pose embedding (a.k.a. feature vector) for pose classification"""
#1. Moving the pose center to the origin
def get_center_point(landmarks, left_bodypart, right_bodypart):
    """Calculate the center point of the two given landmarks"""
    left = tf.gather(landmarks, left_bodypart.value, axis=1)
    right = tf.gather(landmarks, right_bodypart.value, axis=1)
    center = left * 0.5 + right * 0.5
    return center

def get_pose_size(landmarks, torso_size_multiplier=2.5):
    """Calculate pose size
    This is the maximum of two values:
        * Torso size multiplied by torso_size_multiplier
        * Maximum distance from pose center to any pose landmark
    """
    #Hips center
    hips_center = get_center_point(landmarks, BodyPart.LEFT_HIP,
                                   BodyPart.RIGHT_HIP)
    #Shoulders center
    shoulders_center = get_center_point(landmarks, BodyPart.LEFT_SHOULDER,
                                        BodyPart.RIGHT_SHOULDER)
    #Torso size as the minimum body size
    torso_size = tf.linalg.norm(shoulders_center - hips_center)

    #Pose center
    pose_center_new = get_center_point(landmarks, BodyPart.LEFT_HIP,
                                       BodyPart.RIGHT_HIP)
    pose_center_new = tf.expand_dims(pose_center_new, axis=1)
    #Broadcast pose center to the same size as the landmark vector to perform subtraction
    pose_center_new = tf.broadcast_to(pose_center_new,
                                      [tf.size(landmarks) // (17*2), 17, 2])
    #Distance to pose center
    d = tf.gather(landmarks - pose_center_new, 0, axis=0,
                  name="dist_to_pose_center")
    #Max distance to pose center
    max_dist = tf.reduce_max(tf.linalg.norm(d, axis=0))
    #Normalize scale
    pose_size = tf.maximum(torso_size * torso_size_multiplier, max_dist)

    return pose_size

def normalize_pose_landmarks(landmarks):
    """Normalize the landmarks translation by moving the pose center to (0,0) and scaling it to a constant pose size"""
    #Move landmarks so that the pose center becomes (0,0)
    pose_center = get_center_point(landmarks, BodyPart.LEFT_HIP,
                                   BodyPart.RIGHT_HIP)
    pose_center = tf.expand_dims(pose_center, axis=1)
    #Broadcast pose center to same size as landmark vector to perform subtraction
    pose_center = tf.broadcast_to(pose_center,
                                  [tf.size(landmarks) // (17*2), 17, 2])
    landmarks = landmarks - pose_center

    #scale the landmarks to a constant pose size
    pose_size = get_pose_size(landmarks)
    landmarks /= pose_size

    return landmarks

def landmarks_to_embedding(landmarks_and_scores):
    """Convert the input landmarks into a pose embedding"""
    #Reshape flat input into matrix with shape=(17, 3)
    reshaped_inputs = keras.layers.Reshape((17, 3))(landmarks_and_scores)

    #normalize landmarks 2D
    landmarks = normalize_pose_landmarks(reshaped_inputs[:, :, :2])

    #flatten the normalized landmark coordinates into a vector
    embedding = keras.layers.Flatten()(landmarks)
    return embedding

"""Define a Keras model for pose classification"""
#Define model
inputs = tf.Keras.Input(shape=(51))
embedding = landmarks_to_embedding(inputs)

layer = keras.layers.Dense(128, activation=tf.nn.relu6)(embedding)
layer = keras.layers.Dropout(0.5)(layer)
layer = keras.layers.Dense(64, activation=tf.nn.relu6)(layer)
layer = keras.layers.Dropout(0.5)(layer)
outputs = keras.layers.Dense(len(class_names), activation="softmax")(layer)

model = keras.Model(inputs, outputs)
model.summary()

model.compile(
    optimizer='adam',
    loss='categorical_crossentropy',
    metrics=['accuracy']
)

#Add checkpoint callback to store the checkpoint that has the highest validation accuracy
checkpoint_path = "weights.best.hdf5"
checkpoint = keras.callbacks.ModelCheckpoint(checkpoint_path,
                                             monitor='val_accuracy',
                                             verbose=1,
                                             save_best_only=True,
                                             mode='max')
earlystopping = keras.callbacks.EarlyStopping(monitor='val_accuracy',
                                              patience=20)

#Start training
history = model.fit(X_train, y_train,
                    epochs=200,
                    batch_size=16,
                    validation_data=(X_val, y_val),
                    callbacks=[checkpoint, earlystopping])

#Visualize the training history to see if we're overfitting
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('Model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['TRAIN', 'VAL'], loc='lower right')
plt.show()

#Evaluate the model using TEST dataset
loss, accuracy = model.evaluate(X_test, y_test)

"""Draw the confusion matrix to better understand the model performance"""
def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """Plots the confusion matrix"""
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print("Confusion matrix, without normalization")

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=55)
    plt.yticks(tick_marks, classes)
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()

#Classify pose in the TEST dataset using the trained model
y_pred = model.predict(X_test)

#convert the prediction result to class name
y_pred_label = [class_names[i] for i in np.argmax(y_pred, axis=1)]
y_true_label = [class_names[i] for i in np.argmax(y_test, axis=1)]

#plot the confusion matrix
cm = confusion_matrix(np.argmax(y_test, axis=1), np.argmax(y_pred, axis=1))
plot_confusion_matrix(cm,
                      class_names,
                      title = 'Confusion Matrix of Pose Classification Model')

#print the classification report
print('\nClassification Report: \n', classification_report(y_true_label,
                                                           y_pred_label))

"""Investigate incorrect predictions"""
if is_skip_step_1:
    raise RuntimeError('Step 1 needs to be executed to run this cell')

IMAGE_PER_ROW = 3
MAX_NO_OF_IMAGE_TO_PLOT = 30

#extract list of incorrectly predicted poses
false_predict = [id_in_df for id_in_df in range(len(y_test)) \
                 if y_pred_label[id_in_df] != y_true_label[id_in_df]]
if len(false_predict) > MAX_NO_OF_IMAGE_TO_PLOT:
    false_predict = false_predict[:MAX_NO_OF_IMAGE_TO_PLOT]

#plot the incorrectly predicted images
row_count = len(false_predict) // IMAGE_PER_ROW + 1
fig = plt.figure(figsize=(10 * IMAGE_PER_ROW, 10 * row_count))
for i, id_in_df in enumerate(false_predict):
    ax = fig.add_subplot(row_count, IMAGE_PER_ROW, i + 1)
    image_path = os.path.join(images_out_test_folder,
                              df_test.iloc[id_in_df]['file name'])
    image = cv2.imread(image_path)
    plt.title("Predict: %s; Actual: %s"
              % (y_pred_label[id_in_df], y_true_label[id_in_df]))
    plt.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
    plt.show()

"""Convert the pose classification model to TensorFlow Lite"""
converter = tf.lite.TFLiteConverter.from_keras_model(model)
converter.optimizations = [tf.lite.Optimize.DEFAULT]
tflite_model = converter.convert()

print('Model size: %dKB' % (len(tflite_model) / 1024))
with open('pose_classifier.tflite', 'wb') as f:
    f.write(tflite_model)
with open('pose_labels.txt', 'w') as f:
    f.write('\n'.join(class_names))

def evaluate_model(interpreter, X, y_true):
    """Evaluates the given TFLite model and return its accuracy"""
    input_index = interpreter.get_input_details()[0]["index"]
    output_index = interpreter.get_output_details()[0]["index"]

    #Run predictions on all given poses
    y_pred = []
    for i in range(len(y_true)):
        #Pre-processing: add batch dimenstion an convert to float32 to match the models input data format
        test_image = X[i: i + 1].astype('float32')
        interpreter.set_tensor(input_index, test_image)

        #run inference
        interpreter.invoke()

        #Post-processing: remove batch dimension and find the class with highest probability
        output = interpreter.tensor(output_index)
        predicted_label = np.argmax(output()[0])
        y_pred.append(predicted_label)
    #Compare prediction results with ground truth labels to calculate accuracy
    y_pred = keras.utils.to_categorical(y_pred)
    return accuracy_score(y_true, y_pred)

#Evaluate the accuracy of the converted TFLite model
classifier_interpreter = tf.lite.Interpreter(model_content=tflite_model)
classifier_interpreter.allocate_tensors()
print('Accuracy of TFLite model: %s' %
      evaluate_model(classifier_interpreter, X_test, y_test))
