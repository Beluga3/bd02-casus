import numpy as np
import tensorflow as tf
import glob
import csv
import matplotlib.pyplot as plt

class TestModel(tf.Module):
    IMG_SIZE=28
    def __init__(self):
        super(TestModel, self).__init__()
        self.model = tf.keras.Sequential([
            tf.keras.layers.Flatten(input_shape=(IMG_SIZE, IMG_SIZE), name='flatten'),
            tf.keras.layers.Dense(128, activation='relu', name='dense_1'),
            tf.keras.layers.Dense(10, name='dense_2')
            ])
        self.model.compile(
            optimizer='sgd',
            loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True))

    @tf.function(input_signature=[tf.TensorSpec(shape=[1,10], dtype=tf.float32)])
    def add(self, x):
        return {'result' : x + 4}   #<---

    #the 'train' function takes a batch of input images and labels
    @tf.function(input_signature=[
        tf.TensorSpec([None, IMG_SIZE, IMG_SIZE], tf.float32),
        tf.TensorSpec([None, 10], tf.float32),
    ])
    def train(self, x, y):
        with tf.GradientTape() as tape:
            prediction = self.model(x)
            loss = self.model.loss(y, prediction)
        gradients = tape.gradient(loss, self.model.trainable_variables)
        self.model.optimizer.apply_gradients(
            zip(gradients, self.model.trainable_variables))
        result = {"loss: ": loss}
        return result

    @tf.function(input_signature=[
        tf.TensorSpec([None, IMG_SIZE, IMG_SIZE], tf.float32),
    ])
    def infer(self, x):
        logits = self.model(x)
        probabilities = tf.nn.softmax(logits, axis=-1)
        return {
            "output": probabilities,
            "logits": logits
        }

    @tf.function(input_signature=[tf.TensorSpec(shape=[], dtype=tf.string)])
    def save(self, checkpoint_path):
        tensor_names = [weight.name for weight in self.model.weights]
        tensors_to_save = [weight.read_value() for weight in self.model.weights]
        tf.raw_ops.Save(
            filename=checkpoint_path, tensor_names=tensor_names, data=tensors_to_save, name='save')
        return {
            "checkpoint_path": checkpoint_path
        }

    @tf.function(input_signature=[tf.TensorSpec(shape=[], dtype=tf.string)])
    def restore(self, checkpoint_path):
        restored_tensors = {}
        for var in self.model.weights:
            restored = tf.raw_ops.Restore(
                file_pattern=checkpoint_path, tensor_name=var.name, dt=var.dtype, name='restore')
            var.assign(restored)
            restored_tensors[var.name] = restored
        return restored_tensors

#save the model
def saveModel(m):
    SAVED_MODEL_PATH = 'content/pose_landmark_full_new'
    tf.saved_model.save(
        m,
        SAVED_MODEL_PATH,
        signatures={
            'train':
                m.train.get_concrete_function(),
            'infer':
                m.infer.get_concrete_function(),
            'save':
                m.save.get_concrete_function(),
            'restore':
                m.restore.get_concrete_function(),
        })
    #convert the model
    converter = tf.lite.TFLiteConverter.from_saved_model(SAVED_MODEL_PATH)
    converter.target_spec.supported_ops = [
        tf.lite.OpsSet.TFLITE_BUILTINS, #TensorFlow Lite Built-in ops
        tf.lite.OpsSet.SELECT_TF_OPS    #enable TensorFlow ops
    ]
    converter.experimental_enable_resource_variables = True
    tflite_model = converter.convert

    interpreter = tf.lite.Interpreter(model_content=tflite_model)
    interpreter.allocate_tensors()

    infer = interpreter.get_signature_runner("infer")

def openModel():
    SAVED_MODEL_PATH = 'content/pose_landmark_full'
    TFLITE_FILE_PATH = 'content/pose_landmark_full.tflite'
    module = TestModel()
    tf.saved_model.save(
        module, SAVED_MODEL_PATH,
        signatures={'my_signature':module.add.get_concrete_function()})

    #convert the model
    converter = tf.lite.TFLiteConverter.from_saved_model(SAVED_MODEL_PATH)
    tflite_model = converter.convert()
    with open(TFLITE_FILE_PATH, 'wb') as f:
        f.write(tflite_model)
    return tflite_model

def loadTrainingData(path_to_images, path_to_labels): #image foldername - labels.csv filename
    #images
    filelist = glob.glob(path_to_images+'*.bmp')
    imagearray = np.array([np.array(Image.open(fname)) for fname in filelist])
    imagearray.dump('image_nparray.npy') #to load: x = np.load('image_nparray.npy')
    #labels
    with open(path_to_labels+'labels.csv', newline='') as f:
        reader = csv.reader(f)
        labels = list(reader)
    return imagearray, labels

#========================================================================
#load TFLite model in TFLite interpreter
#TFLITE_FILE_PATH = 'content/pose_landmark_full.tflite'
#interpreter = tf.lite.Interpreter(TFLITE_FILE_PATH)
#interpreter.allocate_tensors()

#------------------------------------------------------------------------
#get signature by name
#my_signature = interpreter.get_signature_runner()

#get input and output tensors
#input_details = interpreter.get_input_details()
#output_details = interpreter.get_output_details()

#test model on random input data
#input_shape = input_details[0]['shape']
#input_data = np.array(np.random.random_sample(input_shape),dtype=np.float32)
#interpreter.set_tensor(input_details[0]['index'], input_data)
#interpreter.invoke()
#output_data = interpreter.get_tensor(output_details[0]['index'])
#print(output_data)
#==========================================================================

#preprocess new dataset
p = r"C:\\Users\\Beluga\\Documents\\school\\Jaar 4\\BD02 AI\\Opdrachten\\Casus\\TFModel\\Data\\"
train_images, train_labels = loadTrainingData(p+'Images', p)
print("Preprocessing the dataset")
train_images = (train_images / 255.0).astype(np.float32)
train_labels = tf.keras.utils.to_categorical(train_labels)
print("Passed getting trainingdata")

print("Start training")
NUM_EPOCHS = 100
BATCH_SIZE = 100
epochs = np.arange(1, NUM_EPOCHS + 1, 1)
losses = np.zeros([NUM_EPOCHS])
m = Model()

train_ds = tf.data.Dataset.from_tensor_slices((train__images, train_labels))
train_ds = train_ds.batch(BATCH_SIZE)

for i in range(NUM_EPOCHS):
    print("Epoch {0}".format(str(i)))
    for x,y in train_ds:
        result = m.train(x, y)

    losses[i] = result['loss']
    if (i+1) % 10 == 0:
        print(f"Finished {i+1} epochs")
        print(f"    Loss: {losses[i]:.3f}")

#save trained weights to a checkpoint
m.save('/tmp/model.ckpt')
plt.plot(epochs, losses, label="Pre-training")
plt.ylim([0, max(plt.ylim())])
plt.xlabel('Epoch')
plt.ylabel('Loss [Cross Entropy]')
plt.legend()
print("Passed training")

#save model

